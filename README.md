jquery.draggable
=========================

a plugin to make elements draggable



Example
=========================


> index.html
```html
<style>.div{width: 100px; height: 100px; background-color: gray;}</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="jquery.draggable.js"></script>
<script src="script.js"></script>
<div id="drag-me">DRAG ME</div>
<div id="change-on-drag">DRAG ME TO CHANGE ME</div>
```

> script.js
```javascript
$('#drag-me').draggable();
$('#change-on-drag).draggable(function(self){
    self.html('THIS IS A NEW TEXT');
});
```
