(function ($){
    var drag = {
	target: null,
	x: 0,
	y: 0,
	state: false,
	zindex: 0,
    };

    var delta = {
	x: 0,
	y: 0,
    }

    var zindex = 10;

    function mousedown(e){
	var $target = $(e.target);

	if(!$target.hasClass('draggable')){
	    console.log('NOOT');
	    return;
	}
	drag.state = true;
	drag.target = $target;
	drag.target.addClass('dragging-active')
	drag.x = e.pageX;
	drag.y = e.pageY;

	drag.target.css('z-index', zindex++);

	e.preventDefault();
    }

    $(window).on('mouseup', function(e){
	if(drag.state){
	    drag.state = false;
	    drag.target.removeClass('dragging-active')

	    var dropAction = drag.target.data('dropAction');
	    dropAction && dropAction();

	}
    });

    $(window).on('mousemove', function(e){
	if(drag.state){
	    delta.x = e.pageX - drag.x;
	    delta.y = e.pageY - drag.y;

	    var off = drag.target.offset();
	    drag.target.offset({left: off.left+delta.x, top: off.top+delta.y});

	    drag.x = e.pageX;
	    drag.y = e.pageY;
	}
    });


    $.fn.draggable = function(dropAction){


	return this.each(function(){

	    var $this = $(this).addClass('draggable');
	    $this.css('z-index', zindex++);

	    $this.on('mousedown', mousedown);
	    if(typeof dropAction === 'function')
		$this.data('dropAction', function(){
		    return dropAction($this);
		});
	});
    };


})( jQuery );
